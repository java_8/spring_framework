<center><img src="./pictures/Spring-Logo.png" alt="Spring logo" width="200"/></center>

# SPRINT FRAMEWORK
## Índice ##
1. [¿Qué es Sprint?](#id1)
2. [¿Qué función tiene Spring?](#id2)
3. [Módulos](#id3)

<a name="id1"></a>
# ¿Qué es Sprint?

Spring es un marco de trabajo para crear aplicaciones Web de alto rendimiento mediante el Lenguaje de Programación Java. Al igual que otros Frameworks, Spring permite reutilizar módulos y evitar la escritura de código en exceso, permitiendo así que el Desarrollador se enfoque en el proceso de la aplicación.

<a name="id2"></a>
## ¿Qué función tiene Spring?

En la mayoría de las ocasiones para desarrollar la aplicación que necesitamos no nos es suficiente con usar un único framework sino que necesitamos utilizar varios frameworks. Cada uno de los cuales generará su propio conjunto de objetos.
<center>![frameworks](./pictures/frameworks.gif)</center>

Esta situación genera problemas ya que cada framework es totalmente independiente y gestiona su propio ciclo de vida de los objetos.

Spring ayuda a solventar este problema ya que cambia las responsabilidades y en vez de que el propio desarrollador sea el encargado de generar los objetos de cada uno de los frameworks es Spring basandose en ficheros xml o anotaciones el encargado de construir todos los objetos que la aplicación va a utilizar.

<center>![Spring framework](./pictures/spring_framework.gif)</center>

De esta manera al ser Spring el encargado de inicializar todos los objetos de los distintos frameworks, es también el responsable de asegurarnos que se integran de la forma correcta.
<center>![Spring framework](./pictures/spring_integracion.gif)</center>

<a name="id3"></a>
# Módulos

Dentro del paquete estándar de Spring Framework podemos encontrar el siguiente conjunto de módulos, con el siguiente esquema general.

<center>![Módulos de Spring](./pictures/modulos_spring.gif)</center>

Spring Framework comprende diversos módulos que proveen un rango de servicios:
- **Contenedor de inversión de control**: permite la configuración de los componentes de aplicación y la administración del ciclo de vida de los objetos Java, se lleva a cabo principalmente a través de la inyección de dependencias.
- **Programación orientada a aspectos**: habilita la implementación de rutinas transversales.
- **Acceso a datos**: se trabaja con RDBMS en la plataforma java, usando Java Database Connectivity y herramientas de Mapeo objeto relacional con bases de datos NoSQL.
- **Gestión de transacciones**: unifica distintas APIs de gestión y coordina las transacciones para los objetos Java.
- **Modelo vista controlador**: Un framework basado en HTTP y servlets, que provee herramientas para la extensión y personalización de aplicaciones web y servicios web REST.
- **Framework de acceso remoto**: Permite la importación y exportación estilo RPC, de objetos Java a través de redes que soporten RMI, CORBA y protocolos basados en HTTP incluyendo servicios web (SOAP).
- **Convención sobre Configuración**: el módulo Spring Roo ofrece una solución rápida para el desarrollo de aplicaciones basadas en Spring Framework, privilegiando la simplicidad sin perder flexibilidad.
- **Procesamiento por lotes**: un framework para procesamiento de mucho volumen que como características incluye funciones de registro/trazado, manejo de transacciones, estadísticas de procesamiento de tareas, reinicio de tareas, y manejo de recursos.
- **Autenticación y Autorización**: procesos de seguridad configurables que soportan un rango de estándares, protocolos, herramientas y prácticas a través del subproyecto Spring Security (antiguamente Acegi).
- **Administración Remota**: Configuración de visibilidad y gestión de objetos Java para la configuración local o remota vía JMX.
- **Mensajes**: Registro configurable de objetos receptores de mensajes, para el consumo transparente desde la a través de JMS, una mejora del envío de mensajes sobre las API JMS estándar.
- **Testing**: Soporte de clases para desarrollo de unidades de prueba e integración.

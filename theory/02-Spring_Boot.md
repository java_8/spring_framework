
<center><img src="./pictures/Spring-Logo.png" alt="Spring logo" width="200"/></center>

# SPRING BOOT #

Spring Boot es una herramienta que nace en 2012 con la finalidad de simplificar aun más el desarrollo de aplicaciones basadas en el ya popular framework Spring.
Spring Boot busca que el desarrollador solo se centre en el desarrollo de la solución, olvidándose por completo de la compleja configuración que actualmente tiene Spring para poder funcionar.

Las **características de Spring Boot** pueden resumirse de la siguiente manera:
 - **Incorporación directa de aplicaciones de servidores web/contenedores** como Apache Tomcat o Jetty, eliminando la necesidad de incluir archivos WAR (Web Application Archive).
 - **Simplificación de la configuración de Maven** gracias a los POM (Project Object Models) “starter”.
 - **Configuración automática de Spring** en la medida de lo posible
Características no funcionales, como métricas o configuraciones externalizadas

## Spring Initializr
[![Spring Initializr](./pictures/Spring_Boot.PNG)](https://start.spring.io/)



